import react from '@vitejs/plugin-react';
import path from 'path';
import { visualizer } from 'rollup-plugin-visualizer';
import { defineConfig } from 'vite';

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [react(), visualizer()],
    build: {
        lib: {
            entry: path.resolve(__dirname, 'lib', 'index.ts'),
            name: 'yk-components',
            fileName: 'yk-components',
        },
        rollupOptions: {
            external: [
                'react',
                'react-dom',
                'antd',
                'dayjs',
                'react/jsx-runtime',
            ],
            output: {
                globals: {
                    react: 'React',
                },
            },
        },
    },
    server: {
        port: 3000,
    },
});

