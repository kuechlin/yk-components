# yk components

a set of components build with ant design.

-   [Storybook](https://kuechlin.gitlab.io/yk-components)

## Ant advanced table

Ant Design Table with search, select, number, datetime filter

**Example:**

    <AdvancedTable<TestClass>
        dataSource={[]}
        size="small"
        columns={[{
            title: 'Text',
            dataIndex: 'text',
            filter: 'search',
        }, {
            title: 'Weekday',
            dataIndex: 'weekday',
            filter: 'select'
        }, {
            title: 'Amount',
            dataIndex: 'amount',
            filter: 'number'
        }, {
            title: 'Date',
            dataIndex: 'date',
            filter: 'datetime
        }]}
    />

### Api

#### AdvancedTableProps

| Name                                                          | Type                | Required | Description                                            |
| ------------------------------------------------------------- | ------------------- | -------- | ------------------------------------------------------ |
| dataSource                                                    | T[]                 | true     | DataSource                                             |
| columns                                                       | ColumnOptions\<T>[] | true     | Column configuration                                   |
| defaultSelected                                               | string[]            | false    | Default selected columns                               |
| actionColumn                                                  | ColumnProps\<T>     | false    | Action column will be appended to column configuration |
| storage                                                       | StorageConfig       | false    | configure if settings should be save to local storage  |
| [Ant Table Props](https://ant.design/components/table/#Table) | ...                 | ...      |

#### ColumnOptions\<T>

| Name                                                            | Type                                     | Required | Description                                     |
| --------------------------------------------------------------- | ---------------------------------------- | -------- | ----------------------------------------------- | ---------- | ----- | ----------------- |
| title                                                           | string                                   | true     | column title                                    |
| dataIndex                                                       | string                                   | true     | column identifier, must be unique               |
| hidden                                                          | boolean                                  | false    | Default: false                                  |
| sorter                                                          | false                                    | false    | sorter for column, Default: true                |
| filter                                                          | "select"                                 | "search" | "number"                                        | "datetime" | false | filter for column |
| value                                                           | (r: T) => any                            | false    | value selector, Default: get(record, dataIndex) |
| renderToString                                                  | (v: any, record: T, i: number) => string | false    | render value as string for export and select    |
| [Ant Column Props](https://ant.design/components/table/#Column) | ...                                      | ...      |

## AsyncButton

Displays loading spinner when onClick returns a Promise

**Example:**

    <AsyncButton {...defaultAntButtonProps} />

## Advanced Form

Form Items for Ant Desing Form

**Example:**

    <AdvancedForm
        onSubmit={handleSave}
        initialValues={{
            text: 'text',
            number: 10,
            date: moment(new Date()),
            dateTime: moment(new Date()),
            time: moment(new Date()),
            select: true,
            custom: true
        }}
    >
        <AdvancedForm.Group
            items={[{
                title: 'Text',
                type: 'string',
                dataIndex: 'text',
                rules: [{ required: true }]
            }, {
                type: 'number',
                title: 'Number',
                dataIndex: 'number',
                format: '0.0,0'
            }, {
                type: 'date',
                title: 'Date',
                dataIndex: 'date',
                variant: 'date'
            }, {
                type: 'date',
                title: 'Time',
                dataIndex: 'time',
                variant: 'time'
            }, {
                type: 'date',
                title: 'Date Time',
                dataIndex: 'dateTime',
                variant: 'dateTime'
            }, {
                type: 'select',
                title: 'Select',
                dataIndex: 'select',
                options: [
                    { value: true, label: 'Ja' },
                    { value: false, label: 'Nein' }
                ]
            }, {
                type: 'radio',
                title: 'Radio',
                dataIndex: 'radio',
                options: [
                    { value: true, label: 'Ja' },
                    { value: false, label: 'Nein' }
                ]
            }, {
                type: 'custom-updatable',
                shouldUpdate: (prev, next) => prev.custom !== next.custom,
                render: () => <Form.Item
                    name="custom"
                    valuePropName="checked"
                    children={<Checkbox>Custom</Checkbox>}
                />
            }]}
        />
    </AdvancedForm>

### Api

#### AdvancedFormProps\<T>

| Name          | Type                           | Required | Default   |
| ------------- | ------------------------------ | -------- | --------- |
| onSubmit      | (values: any) => Promise\<any> | false    | undefined |
| initialValues | T                              | false    | undefined |
| cancelText    | string                         | false    | 'Cancel'  |
| submitText    | string                         | false    | 'Save'    |
| onCancel      | () => void                     | false    | undefined |
| showButtons   | boolean                        | false    | true      |

#### FormItemOptions

##### BaseFormProps

| Name      | Type                | Required | Description        |
| --------- | ------------------- | -------- | ------------------ | ---------------- |
| title     | string              | true     | Title of form item |
| dataIndex | string              | string[] | true               | name of property |
| hidden    | boolean             | false    | is item hidden     |
| rules     | Rule[]              | false    | validation rules   |
| style     | React.CSSProperties | false    | custom css         |

##### FormStringProps

| Name     | Type                      | Required | Description                           |
| -------- | ------------------------- | -------- | ------------------------------------- |
| textarea | { row?: number; } \| true | false    | should input be displayed as textarea |

##### FormNumberProps

| Name      | Type   | Required | Description         |
| --------- | ------ | -------- | ------------------- |
| precision | number | false    | percision of number |

##### FormSelectProps

| Name    | Type                                      | Required | Description    |
| ------- | ----------------------------------------- | -------- | -------------- |
| options | { value: any; label: React.ReactNode; }[] | true     | select options |

##### FormCheckboxProps

| Name         | Type            | Required | Description                     |
| ------------ | --------------- | -------- | ------------------------------- |
| checkboxText | React.ReactNode | false    | text displayed next to checkbox |

##### FormSwitchProps

| Name      | Type            | Required | Description                            |
| --------- | --------------- | -------- | -------------------------------------- |
| trueText  | React.ReactNode | false    | text display when switch is active     |
| falseText | React.ReactNode | false    | text display when switch is not active |

##### FormRadioProps

| Name    | Type                                      | Required | Description       |
| ------- | ----------------------------------------- | -------- | ----------------- |
| options | { value: any; label: React.ReactNode; }[] | true     | radio options;    |
| variant | "button" \| "checkbox"                    | false    | Default: "button" |

##### FormDateProps

| Name    | Type                           | Required | Description                      |
| ------- | ------------------------------ | -------- | -------------------------------- |
| variant | "date" \| "time" \| "dateTime" | false    | variant of date, default: "date" |

##### FormCustomProps

| Name   | Type                     | Required | Description     |
| ------ | ------------------------ | -------- | --------------- |
| render | () => React.ReactElement | true     | render function |

##### FormCustomUpdatableProps

| Name         | Type                                           | Required | Description                  |
| ------------ | ---------------------------------------------- | -------- | ---------------------------- |
| shouldUpdate | (prev: any, next: any) => boolean              | true     | should update check funktion |
| render       | (instance: FormInstance) => React.ReactElement | true     | render function              |

## Formatter

### Date Formatter

#### set global config

    dateFormatter.configure(options: Partial<DateFormatterConfig>): void;

#### get format for type

    dateFormatter.getFormat(type: "date" | "time" | "dateTime" | string): string;

#### format number

    dateFormatter.format(val: any, type: "date" | "time" | "dateTime" | string = "date"): string;

#### DateFormatterConfig

| Name       | Type                  | Default |
| ---------- | --------------------- | ------- | ------------------------------------------------------------------ |
| emptyValue | string                | -       |
| types      | Record<string,string> | false   | date: "DD.MM.YYYY", time: "HH:mm", dateTime: "DD.MM. YYYY - HH:mm" |

### Number Formatter

#### set global config

    numberFormatter.configure(options: Partial<NumberFormatterConfig>): void;

#### get item from config

    numberFormatter.getConfig(key: keyof NumberFormatterConfig): string;

#### format number

    numberFormatter.format(val: any, percision: number = 3): string;

#### trim zero at end

    numberFormatter.trim(val: string): string;

#### parse formatted number

    numberFormatter.parse(val: any): number;

#### NumberFormatterConfig

| Name               | Type   | Default |
| ------------------ | ------ | ------- |
| decimalSeparator   | string | .       |
| thousandsSeparator | string | ,       |
| emptyValue         | string | -       |

## ThemeProvider

Theme Provider for Ant Design

**Example:**

    <ThemeProvider
        defaultTheme="dark"
        themes={{
            dark: ["/antd.dark.min.css", "/scrollbar.dark.css"],
            light: ["/antd.min.css", "/scrollbar.light.css"]
        }}
        themeVariables={{
            light: {
                primary: '#69c0ff'
            },
            dark: {
                primary: '#164c7e'
            }
        }}
        spinner={<h1 style={{ margin: 'auto 100px' }} children="Loading..." />}
    >
        {children}
    </ThemeProvider>

### Api

| Name           | Type                                    | Required | Description                                            |
| -------------- | --------------------------------------- | -------- | ------------------------------------------------------ |
| themes         | Record<string, string \| string[]>      | true     | theme name to url mapping, multiple urls are supported |
| themeVariables | Record<keyof T, Record\<string,string>> | false    | theme variables                                        |
| defaultTheme   | string                                  | true     | default theme                                          |
| spinner        | React.ReactNode                         | true     | loading spinner for theme change                       |
| children       | React.ReactNode                         | true     | children of theme context                              |

### CSS

-   i have created styles for scrollbars in chrome, dark an light variant.
-   `build/css/scrollbar.light.css`
-   `build/css/scrollbar.dark.css`

## Commands

### Start storybook

    yarn start

### Build

    yarn build
