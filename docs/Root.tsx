import { Card, ConfigProvider, Layout, Menu, theme, Typography } from 'antd';
import React, { useState } from 'react';
import { ThemeToggle, ThemeType } from '../lib';
import { AdvancedDescriptionsExample } from './AdvancedDescriptionsExample';
import { AdvancedTableExample } from './AdvancedTableExample';
import { oneDarkTheme } from './theme';

const { useToken } = theme;

export default function Root() {
    const [theme, setTheme] = useState<ThemeType>('dark');
    return (
        <ConfigProvider
            theme={{
                algorithm: theme === 'dark' ? oneDarkTheme : undefined,
            }}
        >
            <App theme={theme} onSetTheme={setTheme} />
        </ConfigProvider>
    );
}

function App({
    theme,
    onSetTheme,
}: {
    theme: ThemeType;
    onSetTheme(value: ThemeType): void;
}) {
    const [state, setState] = useState('0');

    const { token } = useToken();

    return (
        <Layout style={{ height: '100vh', width: '100vw' }}>
            <Layout.Header
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    gap: 24,
                    backgroundColor: token.colorBgContainer,
                    height: '48px',
                }}
            >
                <Typography.Title
                    style={{
                        margin: 0,
                    }}
                    level={3}
                    children="yk-components"
                />
                <Menu
                    mode="horizontal"
                    selectedKeys={[state]}
                    onSelect={({ key }) => setState(key as string)}
                    style={{ lineHeight: '48px', flexGrow: 1 }}
                    items={[
                        { label: 'Advanced Table', key: '0' },
                        { label: 'Advanced Descriptions', key: '1' },
                    ]}
                />
                <ThemeToggle theme={theme} onSwitch={onSetTheme} />
            </Layout.Header>
            <Layout.Content
                style={{
                    height: '100%',
                    padding: '16px',
                    width: 'calc(100vw)',
                    overflowX: 'hidden',
                    backgroundColor: theme === 'dark' ? '#070707' : undefined,
                }}
            >
                <Card size="small">
                    {state === '0' && <AdvancedTableExample />}
                    {state === '1' && <AdvancedDescriptionsExample />}
                </Card>
            </Layout.Content>
        </Layout>
    );
}
