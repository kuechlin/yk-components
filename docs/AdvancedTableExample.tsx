import React, { useState } from 'react';

import {
    DeleteOutlined,
    DownloadOutlined,
    EditOutlined,
    SelectOutlined,
    SettingOutlined,
} from '@ant-design/icons';
import { Button, Col, Dropdown, Row, Tabs, Typography } from 'antd';
import { Chance } from 'chance';
import dayjs from 'dayjs';
import { AdvancedTable } from '../lib/AdvancedTable/AdvancedTable';
import { useAdvancedTable } from '../lib/AdvancedTable/useAdvancedTable';
import { renderDate, renderNumber } from '../lib/Formatter';

export const AdvancedTableExample = () => {
    return (
        <Row justify="center">
            <Col span={12}>
                <Typography.Title style={{ textAlign: 'center' }}>
                    Advanced Table
                </Typography.Title>
            </Col>
            <Col span={24}>
                <Tabs
                    items={[
                        {
                            key: '0',
                            label: 'Full Example',
                            children: <FullExample />,
                        },
                        {
                            key: '1',
                            label: 'DateTime',
                            children: <DateTimeFilterExample />,
                        },
                        {
                            key: '2',
                            label: 'Number',
                            children: <NumberFilterExample />,
                        },
                        {
                            key: '3',
                            label: 'Search',
                            children: <SearchFilterExample />,
                        },
                        {
                            key: '4',
                            label: 'Select',
                            children: <SelectFilterExample />,
                        },
                    ]}
                />
            </Col>
        </Row>
    );
};

const DateTimeFilterExample = () => {
    const chance = new Chance();
    class TestClass {
        id = chance.guid();
        dayjsDate = dayjs(chance.date());
        jsDate = chance.date();
        isoStringDate = chance.date().toISOString();
    }

    const [data] = useState(
        new Array<TestClass>(100)
            .fill(new TestClass())
            .map(() => new TestClass())
    );

    return (
        <AdvancedTable<TestClass>
            dataSource={data}
            size="small"
            columns={[
                {
                    title: 'dayjs Date',
                    dataIndex: 'dayjsDate',
                    filter: 'datetime',
                },
                {
                    title: 'JS Date',
                    dataIndex: 'jsDate',
                    filter: 'datetime',
                },
                {
                    title: 'ISO String Date',
                    dataIndex: 'isoStringDate',
                    filter: 'datetime',
                },
            ]}
        />
    );
};

const NumberFilterExample = () => {
    const chance = new Chance();
    class TestClass {
        id = chance.guid();
        int = chance.integer({ min: -2000, max: 10000 });
        double = chance.floating({ min: -2000, max: 10000 });
        dollar = chance.floating({ min: 1000, max: 10000 });
    }

    const [data] = useState(
        new Array<TestClass>(100)
            .fill(new TestClass())
            .map(() => new TestClass())
    );

    return (
        <AdvancedTable<TestClass>
            dataSource={data}
            size="small"
            columns={[
                {
                    title: 'Int',
                    dataIndex: 'int',
                    filter: 'number',
                },
                {
                    title: 'Double',
                    dataIndex: 'double',
                    filter: 'number',
                    render: renderNumber({ precision: 4 }),
                },
                {
                    title: 'Dollar',
                    dataIndex: 'dollar',
                    filter: 'number',
                    render: renderNumber({ precision: 2, unit: '$' }),
                },
            ]}
        />
    );
};

const SearchFilterExample = () => {
    const chance = new Chance();
    class TestClass {
        id = chance.guid();
        animal = chance.animal();
        company = chance.company();
        paragraph = chance.paragraph().substring(0, 200);
    }

    const [data] = useState(
        new Array<TestClass>(100)
            .fill(new TestClass())
            .map(() => new TestClass())
    );

    return (
        <AdvancedTable<TestClass>
            dataSource={data}
            size="small"
            columns={[
                {
                    title: 'Animal',
                    dataIndex: 'animal',
                    filter: 'search',
                },
                {
                    title: 'Company',
                    dataIndex: 'company',
                    filter: 'search',
                },
                {
                    title: 'Paragraph',
                    dataIndex: 'paragraph',
                    filter: 'search',
                },
            ]}
        />
    );
};

const SelectFilterExample = () => {
    const chance = new Chance();
    class TestClass {
        id = chance.guid();
        coin = chance.coin();
        weekday = chance.weekday({});
        animal = chance.animal();
    }

    const [data] = useState(
        new Array<TestClass>(100)
            .fill(new TestClass())
            .map(() => new TestClass())
    );

    return (
        <AdvancedTable<TestClass>
            dataSource={data}
            size="small"
            columns={[
                {
                    title: 'Coin',
                    dataIndex: 'coin',
                    filter: 'select',
                },
                {
                    title: 'Weekday',
                    dataIndex: 'weekday',
                    filter: 'select',
                },
                {
                    title: 'Animal',
                    dataIndex: 'animal',
                    filter: 'select',
                },
            ]}
        />
    );
};

const FullExample = () => {
    const chance = new Chance();

    class KeyValue {
        key = chance.guid();
        value = chance.letter() + chance.letter();
    }
    const items = new Array(20).fill(0).map(() => new KeyValue());

    class TestClass {
        constructor() {
            var item = chance.pickone(items);
            this.typeId = item.key;
            this.typeName = item.value;
        }
        id = chance.guid();
        idNr = `A-2020-${chance.integer({ min: 0, max: 99 })}`;
        name = chance.name();
        age = chance.age();
        date = dayjs(chance.date());
        typeId: string;
        typeName: string;
        word = chance.word();
        weekday = chance.weekday({});
        birthday = dayjs(chance.birthday());
        gender = chance.gender();
        address = new Address();
        changing = chance.animal();
    }
    class Address {
        city = chance.city();
        bool = chance.bool();
        street = chance.street();
        number = chance.integer({ min: 1, max: 100 });
        date = dayjs(chance.date());
    }

    const { ref, openColumnSelect, getFilteredData, clearAllFilters } =
        useAdvancedTable<TestClass>();
    const [data] = useState(new Array(300).fill(0).map(() => new TestClass()));

    return (
        <AdvancedTable<TestClass>
            ref={ref}
            dataSource={data}
            size="small"
            scroll={{ x: true }}
            actionColumn={{
                dataIndex: 'id',
                title: (
                    <Dropdown
                        menu={{
                            items: [
                                {
                                    key: '0',
                                    icon: <EditOutlined />,
                                    label: 'Customize',
                                    onClick: openColumnSelect,
                                },
                                {
                                    key: '1',
                                    icon: <DownloadOutlined />,
                                    label: 'Get Data',
                                    onClick: () =>
                                        console.log(getFilteredData()),
                                },
                                {
                                    key: '2',
                                    icon: <DeleteOutlined />,
                                    label: 'Clear Fitlers',
                                    onClick: clearAllFilters,
                                },
                            ],
                        }}
                        children={
                            <Button type="link">
                                <SettingOutlined />
                            </Button>
                        }
                    />
                ),
                render: () => (
                    <Button type="link">
                        <SelectOutlined />
                    </Button>
                ),
            }}
            columns={[
                {
                    title: 'IdNr',
                    dataIndex: 'idNr',
                    width: 120,
                    fixed: 'left',
                    filter: 'search',
                },
                {
                    title: 'Some long Text',
                    dataIndex: 'changing',
                    render: (v) => `${v}`,
                    filter: 'search',
                    width: 300,
                },
                {
                    title: 'Name',
                    dataIndex: 'name',
                    render: (v) => '#' + v,
                    filter: 'search',
                },
                {
                    title: 'Alter',
                    dataIndex: 'age',
                    filter: 'number',
                },
                {
                    title: 'Datum',
                    dataIndex: 'date',
                    filter: 'datetime',
                    render: renderDate('datetime'),
                },
                {
                    title: 'CC',
                    dataIndex: 'type--Id',
                    render: (_v, r) => <a>{r.typeName}</a>,
                    value: (r) => r.typeId,
                    filter: {
                        type: 'select',
                        renderOption: (_v, r) => r.typeName,
                    },
                },
                {
                    title: 'Wort',
                    dataIndex: 'word',
                    filter: {
                        type: 'search',
                        noHighlight: true,
                    },
                    render: (v) => (
                        <h3>
                            {v}
                            <em>{v}</em>
                        </h3>
                    ),
                },
                {
                    title: 'Weekday',
                    dataIndex: 'weekday',
                    filter: 'select',
                },
                {
                    title: 'Birthday',
                    dataIndex: 'birthday',
                    filter: 'datetime',
                    render: renderDate(),
                },
                {
                    title: 'Gender',
                    dataIndex: 'gender',
                    filter: 'select',
                },
                {
                    title: 'City',
                    dataIndex: 'address.city',
                    filter: 'search',
                },
                {
                    title: 'Bool',
                    dataIndex: 'address.bool',
                    filter: 'bool',
                    render: (v) => (v ? 'Ja' : 'Nein'),
                },
                {
                    title: 'Street',
                    dataIndex: 'address.street',
                    filter: 'search',
                },
                {
                    title: 'Number',
                    dataIndex: 'address.number',
                    filter: 'number',
                    render: renderNumber({ unit: '€', trim: true }),
                },
                {
                    title: 'Time',
                    dataIndex: 'address.date',
                    render: renderDate('time'),
                },
            ]}
            storage={{
                id: 'demo-table/v2',
            }}
        />
    );
};
