import React from 'react';

import { Col, Row, Tag, Typography } from 'antd';
import { Chance } from 'chance';
import dayjs from 'dayjs';
import { AdvancedDescriptions, columnOptions, numberFormatter } from '../lib';

export const AdvancedDescriptionsExample = () => {
    const chance = new Chance();

    class KeyValue {
        key = chance.guid();
        value = chance.letter() + chance.letter();
    }
    const items = new Array(20).fill(0).map(() => new KeyValue());

    class TestClass {
        constructor() {
            var item = chance.pickone(items);
            this.typeId = item.key;
            this.typeName = item.value;
        }
        id = chance.guid();
        idNr = `A-2020-${chance.integer({ min: 0, max: 99 })}`;
        name = chance.name();
        age = chance.age();
        date = dayjs(chance.date());
        typeId: string;
        typeName: string;
        word = chance.word();
        weekday = chance.weekday({});
        birthday = dayjs(chance.birthday());
        gender = chance.gender();
        address = new Address();
        changing = chance.animal();
    }
    class Address {
        city = chance.city();
        bool = chance.bool();
        street = chance.street();
        number = chance.floating({ min: 1, max: 100 });
        date = dayjs(chance.date());
    }

    return (
        <Row justify="center">
            <Col span={20}>
                <Typography.Title style={{ textAlign: 'center' }}>
                    Advanced Descriptions
                </Typography.Title>

                <AdvancedDescriptions<TestClass>
                    data={new TestClass()}
                    size="small"
                    column={2}
                    columns={[
                        columnOptions<TestClass>({
                            title: 'IdNr',
                            dataIndex: 'idNr',
                            colSpan: 2,
                        }),
                        {
                            title: 'Changing Text',
                            dataIndex: 'changing',
                            render: (v) => <Tag children={v} />,
                        },
                        {
                            title: 'Name',
                            dataIndex: 'name',
                        },
                        {
                            title: 'Alter',
                            dataIndex: 'age',
                        },
                        {
                            title: 'Datum',
                            dataIndex: 'date',
                        },
                        {
                            title: 'CC',
                            dataIndex: 'typeId',
                            render: (_v, r) => r.typeName,
                        },
                        {
                            title: 'Wort',
                            dataIndex: 'word',
                        },
                        {
                            title: 'Weekday',
                            dataIndex: 'weekday',
                        },
                        {
                            title: 'Birthday',
                            dataIndex: 'birthday',
                        },
                        {
                            title: 'Gender',
                            dataIndex: 'gender',
                        },
                        {
                            title: 'City',
                            dataIndex: 'address.city',
                        },
                        {
                            title: 'Bool',
                            dataIndex: 'address.bool',
                            render: (v) => (v ? 'Ja' : 'Nein'),
                        },
                        {
                            title: 'Street',
                            dataIndex: 'address.street',
                        },
                        {
                            title: 'Number',
                            dataIndex: 'address.number',
                            render: (v) => numberFormatter.format(v, 3),
                        },
                        {
                            title: 'Date',
                            dataIndex: 'address.date',
                        },
                    ]}
                />
            </Col>
        </Row>
    );
};
