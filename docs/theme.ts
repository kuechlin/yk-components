import { theme } from 'antd';
import { SeedToken } from 'antd/es/theme';
const { darkAlgorithm } = theme;

export const oneDarkTheme = (seedToken: SeedToken) =>
    darkAlgorithm({
        ...seedToken,
        borderRadius: 2,
        colorBgBase: '#08080a',
        colorError: '#e06c75',
        colorInfo: '#61afef',
        colorPrimary: '#61afef',
        colorSuccess: '#98c379',
        colorTextBase: '#d5d8df',
        colorWarning: '#e5c07b',
    });
