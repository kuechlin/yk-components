import dayjs, { Dayjs } from 'dayjs';

export type DateType = 'date' | 'time' | 'datetime';

export type DateTypesConfig = {
    emptyValue: string;
    formats: Record<DateType, string>;
};

const config: DateTypesConfig = {
    emptyValue: '-',
    formats: {
        date: 'DD.MM.YYYY',
        time: 'HH:mm',
        datetime: 'DD.MM.YYYY - HH:mm',
    },
};

const configure = (options: Partial<DateTypesConfig>) => {
    for (const key in options) {
        if ((options as any)[key]) {
            (config as any)[key] = (options as any)[key] as string;
        }
    }
};

const getFormat = (type: DateType | string) => {
    return (config.formats as Record<string, string>)[type] || type;
};

const format = (val: any, type: DateType | string = 'date') => {
    if (!val) return config.emptyValue;
    return dayjs(val).format(getFormat(type));
};

export const dateFormatter = {
    configure,
    format,
    getFormat,
};

export const renderDate =
    (type: 'date' | 'time' | 'dateTime' | string = 'date') =>
    (val: string | Dayjs | null | undefined) => {
        return format(val, type);
    };
