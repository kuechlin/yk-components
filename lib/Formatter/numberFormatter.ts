export type NumberFormatterConfig = {
    decimalSeparator: string;
    thousandsSeparator: string;
    emptyValue: string;
};

const config: NumberFormatterConfig = {
    decimalSeparator: '.',
    thousandsSeparator: ',',
    emptyValue: '-',
};

const configure = (options: Partial<NumberFormatterConfig>) => {
    for (const key in options) {
        if ((options as any)[key]) {
            (config as any)[key] = (options as any)[key];
        }
    }
};

const getConfig = (key: keyof NumberFormatterConfig) => config[key];

const format = (val: any, precision: number = 3) => {
    if (typeof val === 'string') {
        val = Number.parseFloat(val);
    }
    if (
        val === null ||
        val === undefined ||
        isNaN(val) ||
        typeof val !== 'number'
    )
        return config.emptyValue;

    var value = (val as number).toFixed(precision).split('.');

    const replaced = value[0].replace(
        /\B(?=(\d{3})+(?!\d))/g,
        config.thousandsSeparator
    );

    if (value.length == 2) {
        return replaced + config.decimalSeparator + value[1];
    } else {
        return replaced;
    }
};

const trim = (val: string) => {
    while (val && val[val.length - 1] === '0') {
        val = val.substring(0, val.length - 1);
    }
    if (val.endsWith(config.decimalSeparator)) {
        return val.substring(0, val.length - 1);
    } else {
        return val;
    }
};

const parse = (val: any) => {
    if (typeof val === 'number') return val;
    if (!val || typeof val !== 'string' || val === config.emptyValue)
        return null;
    while (val.includes(config.thousandsSeparator)) {
        val = val.replace(config.thousandsSeparator, '');
    }
    if (config.decimalSeparator !== '.') {
        val = val.replace(config.decimalSeparator, '.');
    }
    return Number.parseFloat(val);
};

export const numberFormatter = {
    configure,
    format,
    parse,
    trim,
    getConfig,
};

type NumberRenderOptions = {
    trim?: boolean;
    precision?: number;
    unit?: string;
};

export const renderNumber =
    ({ trim: _trim, precision, unit }: NumberRenderOptions) =>
    (val: string | number | null | undefined) => {
        let result = format(val, precision);
        if (_trim) {
            result = trim(result);
        }
        if (unit) {
            result += ' ' + unit;
        }
        return result;
    };
