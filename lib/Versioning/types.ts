export type ChangeLogState = {
    version: string;
    logs: VersionChange[];
};

export type VersionChange = {
    version: string;
    date: string;
    description?: string;
    changes: VersionChangeItem[];
};

export type VersionChangeItem = ChangeItem | ChangeGroup;

export type ChangeItem = {
    type: 'bug' | 'feat' | 'removed' | 'important';
    text: string;
};

export type ChangeGroup = {
    type: 'group';
    text: string;
    items: (ChangeItem | ChangeGroup)[];
};
