import {
    AlertOutlined,
    BugOutlined,
    CaretDownOutlined,
    DeleteOutlined,
    PlusOutlined,
} from '@ant-design/icons';
import { Row, Space, Timeline, Tooltip, Typography } from 'antd';
import React from 'react';
import { ChangeGroup, ChangeItem, ChangeLogState } from './types';
const { Text, Title, Paragraph } = Typography;

type ChangeLogProps = {
    changeLog: ChangeLogState;
};
export const ChangeLog = ({ changeLog }: ChangeLogProps) => {
    return (
        <Row justify="center">
            <Timeline style={{ marginTop: '50px', maxWidth: '800px' }}>
                {changeLog.logs.map((item) => (
                    <Timeline.Item key={item.version}>
                        <Space size="large" style={{ marginTop: '-4px' }}>
                            <Title level={3}>Version {item.version}</Title>
                            <Title level={5}>{item.date}</Title>
                        </Space>
                        {item.description && (
                            <Paragraph style={textStyle}>
                                {item.description}
                            </Paragraph>
                        )}
                        <ul style={ulStyle}>
                            {item.changes.map((c, i) => (
                                <ChangeItemComp key={i} change={c} />
                            ))}
                        </ul>
                    </Timeline.Item>
                ))}
            </Timeline>
        </Row>
    );
};
const ulStyle: React.CSSProperties = {
    listStyleType: 'none',
};
const textStyle: React.CSSProperties = {
    lineHeight: '24px',
    fontSize: '16px',
};
const ChangeItemComp = ({ change }: { change: ChangeGroup | ChangeItem }) => {
    if (change.type === 'group')
        return (
            <Space direction="vertical">
                <Space direction="horizontal">
                    <CaretDownOutlined />
                    <Text style={textStyle}>{change.text}</Text>
                </Space>
                <ul style={ulStyle}>
                    {change.items.map((item, i) => (
                        <ChangeItemComp key={i} change={item} />
                    ))}
                </ul>
            </Space>
        );
    else
        return (
            <li>
                <Space>
                    <ChangeTag {...change} />
                    <Text style={textStyle} children={change.text} />
                </Space>
            </li>
        );
};
const icons = {
    bug: <BugOutlined style={{ color: '#d32029' }} />,
    feat: <PlusOutlined style={{ color: 'rgb(23, 125, 220)' }} />,
    important: <AlertOutlined style={{ color: '#d84a1b' }} />,
    removed: <DeleteOutlined />,
    group: <CaretDownOutlined />,
};
const texts = {
    bug: 'Fix',
    feat: 'Feature',
    important: 'Wichtig',
    removed: 'Entfernt',
};
const ChangeTag = ({ type }: ChangeItem) => {
    return (
        <Tooltip title={texts[type]}>
            <Space>{icons[type]}</Space>
        </Tooltip>
    );
};
