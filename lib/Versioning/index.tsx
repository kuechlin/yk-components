import {
    createContext,
    FC,
    ReactNode,
    useContext,
    useEffect,
    useState,
} from 'react';
export { ChangeLog } from './ChangeLog';
export type {
    ChangeGroup,
    ChangeItem,
    ChangeLogState,
    VersionChange,
    VersionChangeItem,
} from './types';

// Production Development Testversion
export type VersionContextState = {
    version: string;
    env: string;
};
const defaultState: VersionContextState = {
    version: '0.0.0',
    env: 'Production',
};
const VersionContext = createContext<VersionContextState>(defaultState);
export const useVersion = () => useContext(VersionContext).version;
export const useEnv = () => useContext(VersionContext).env;

const getLocalVersion = () => localStorage.getItem('version');
const setLocalVersion = (val: string) => localStorage.setItem('version', val);

const upgradeVersion = async (value: string) => {
    if ('caches' in window) {
        const keys = await caches.keys();
        for (const key of keys) {
            await caches.delete(key);
            console.log('deleted cache ' + key);
        }
    }

    sessionStorage.clear();

    setLocalVersion(value);

    window.location.reload();
};

type VersioningProps = {
    children: ReactNode;
    getVersion: () => Promise<{ version: string; env: string }>;
    onUpgrade?(last: string, next: string): void;
};
export const Versioning: FC<VersioningProps> = ({
    children,
    getVersion,
    onUpgrade,
}) => {
    const [state, setState] = useState<VersionContextState>({
        ...defaultState,
        version: getLocalVersion() || defaultState.version,
    });

    useEffect(() => {
        getVersion().then((next) => {
            setState(next);
            if (state.version !== next.version) {
                onUpgrade && onUpgrade(state.version, next.version);
                upgradeVersion(next.version);
            }
        });
    }, []);

    return <VersionContext.Provider value={state} children={children} />;
};
