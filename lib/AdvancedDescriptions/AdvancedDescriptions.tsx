import { Descriptions, DescriptionsProps } from 'antd';
import { ColumnOptions } from '../AdvancedTable';
import { get } from '../utils';

const defaultProps: DescriptionsProps = {
    size: 'small',
    bordered: true,
};

export type AdvancedDescriptionOptions<T> = Pick<
    ColumnOptions<T>,
    'title' | 'dataIndex' | 'hidden' | 'render' | 'colSpan'
>;

export interface AdvancedDescriptionsProps<T>
    extends Omit<DescriptionsProps, 'children'> {
    data: T;
    columns: AdvancedDescriptionOptions<T>[];
}
export function AdvancedDescriptions<T extends Object>({
    data,
    columns,
    ...rest
}: AdvancedDescriptionsProps<T>) {
    return (
        <Descriptions {...defaultProps} {...rest}>
            {columns
                .filter((a) => !a.hidden)
                .map(({ dataIndex, title, colSpan, render }, i) => {
                    let value = get(data, dataIndex);

                    return (
                        <Descriptions.Item
                            key={dataIndex + i}
                            label={title}
                            span={colSpan}
                            children={
                                render ? render(value, data, i) : String(value)
                            }
                        />
                    );
                })}
        </Descriptions>
    );
}
