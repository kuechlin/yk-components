import { InputRef, Radio, Select } from "antd";
import { FilterDropdownProps } from "antd/es/table/interface";
import { useAdvancedTableContext } from "../functions/context";
import { FilterDropdown } from "./FilterDropdown";

interface BoolFilterProps extends FilterDropdownProps {
    dataIndex: string;
}
const { Option } = Select;
export const BoolFilterDropdown = ({
    confirm,
    close,
    clearFilters,
    selectedKeys,
    setSelectedKeys,
    dataIndex,
}: BoolFilterProps) => {
    const [state, setState] = useAdvancedTableContext();

    const handleOk = () => {
        const store = { ...state.store };
        if (store[dataIndex]) {
            store[dataIndex].filters = selectedKeys;
        }
        setState({ store });
        confirm();
    };

    const handleReset = () => {
        clearFilters && clearFilters();
        const store = { ...state.store };
        if (store[dataIndex]) {
            store[dataIndex].filters = null;
        }
        setState({ store });
        close();
    };

    return (
        <FilterDropdown onOk={handleOk} onReset={handleReset}>
            <Radio.Group buttonStyle="solid" value={selectedKeys[0]} onChange={(e) => setSelectedKeys([e.target.value])}>
                <Radio.Button value="true">Ja</Radio.Button>
                <Radio.Button value="false">Nein</Radio.Button>
            </Radio.Group>
        </FilterDropdown>
    );
};
