import { InputNumber, Select, Space } from 'antd';
import { FilterDropdownProps, FilterValue } from 'antd/es/table/interface';
import { useAdvancedTableContext } from '../functions/context';
import { FilterDropdown } from './FilterDropdown';

type NumberFilterSingleOperator = 'eq' | 'gt' | 'lt' | 'neq';
export type NumberFilterState =
    | {
          op: NumberFilterSingleOperator;
          value: number | null;
      }
    | {
          op: 'range';
          values: [number | null, number | null];
      };

export function parseNumberState(value: FilterValue): NumberFilterState | null {
    if (value && value.length > 0) {
        return JSON.parse(String(value[0])) as NumberFilterState;
    } else {
        return null;
    }
}
function toFilterValue(filter: NumberFilterState): string[] {
    return [JSON.stringify(filter)];
}

interface NumberFilterProps extends FilterDropdownProps {
    dataIndex: string;
}
export const NumberFilterDropdown = ({
    confirm,
    close,
    clearFilters,
    selectedKeys,
    setSelectedKeys,
    dataIndex,
}: NumberFilterProps) => {
    const [state, setState] = useAdvancedTableContext();
    const filterValue = parseNumberState(selectedKeys);

    const handleOk = () => {
        confirm();
        const store = { ...state.store };
        if (store[dataIndex]) {
            store[dataIndex].filters = selectedKeys;
        }
        setState({ store });
    };

    const handleReset = () => {
        clearFilters && clearFilters();
        const store = { ...state.store };
        if (store[dataIndex]) {
            store[dataIndex].filters = null;
        }
        setState({ store });
        close();
    };

    const handleChange = (value: NumberFilterState) =>
        setSelectedKeys(toFilterValue(value));

    const handleFilterChange = (
        value: NumberFilterSingleOperator | 'range'
    ) => {
        if (value === 'range') {
            handleChange({
                op: value,
                values: [null, null],
            });
        } else {
            handleChange({
                op: value,
                value: null,
            });
        }
    };

    return (
        <FilterDropdown onOk={handleOk} onReset={handleReset}>
            <Select
                value={filterValue?.op}
                options={[
                    { value: 'eq', label: 'gleich' },
                    { value: 'neq', label: 'nicht gelich' },
                    { value: 'gt', label: 'größer als' },
                    { value: 'lt', label: 'kleiner als' },
                    { value: 'range', label: 'zwischen' },
                ]}
                style={{ width: '100%' }}
                placeholder="Filtertyp"
                onChange={handleFilterChange}
            />
            {filterValue?.op === 'range' ? (
                <Space>
                    <InputNumber
                        value={filterValue.values[0]}
                        placeholder="Von"
                        onChange={(val) =>
                            handleChange({
                                op: 'range',
                                values: [val, filterValue.values[1]],
                            })
                        }
                    />
                    <InputNumber
                        value={filterValue.values[1]}
                        placeholder="Bis"
                        onChange={(val) =>
                            handleChange({
                                op: 'range',
                                values: [filterValue.values[0], val],
                            })
                        }
                    />
                </Space>
            ) : (
                <InputNumber
                    value={filterValue?.value}
                    placeholder="Wert"
                    onChange={(value) =>
                        handleChange({
                            op: filterValue?.op as NumberFilterSingleOperator,
                            value,
                        })
                    }
                    style={{ width: '100%' }}
                />
            )}
        </FilterDropdown>
    );
};
