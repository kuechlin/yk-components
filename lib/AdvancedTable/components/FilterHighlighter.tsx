import { Typography } from 'antd';
import { FilterValue } from 'antd/es/table/interface';
import { ReactNode } from 'react';
import { get, getNodeText } from '../../utils';
import { useAdvancedTableContext } from '../functions/context';

export const FilterHighlighter = ({
    children,
    dataIndex,
}: {
    children: ReactNode;
    dataIndex: string;
}) => {
    const [state] = useAdvancedTableContext();

    const filter = get(state.store, [dataIndex, 'filters']) as FilterValue;
    const caseSensitive = get(state.store, [dataIndex, 'caseSensitive'], false);

    if (!filter || !filter[0]) return <>{children}</>;

    let value = String(filter[0]);
    let nodeText = getNodeText(children);
    let segments = nodeText.split(new RegExp(value, 'i'));
    let indicies = getIndicies(nodeText, value, caseSensitive);
    let highlights = getMarkedTexts(nodeText, indicies, value.length);
    return <>{jsxJoin(segments, highlights)}</>;
};

function getMarkedTexts(text: string, indicies: number[], termLength: number) {
    return indicies.map((x) => (
        <Typography.Text mark children={text.substring(x, x + termLength)} />
    ));
}

function getIndicies(text: string, search: string, caseSensitive: boolean) {
    let indicies: number[] = [];
    let i = 0;
    while (i < text.length) {
        let index = caseSensitive
            ? text.indexOf(search, i)
            : text.toLowerCase().indexOf(search.toLowerCase(), i);
        if (index === -1) break;
        indicies.push(index);
        i = index + search.length;
    }
    return indicies;
}

function jsxJoin(array: ReactNode[], str: ReactNode[]) {
    return array.reduce((acc, x, index) => {
        return acc === null ? (
            x
        ) : (
            <>
                {acc}
                {str[index - 1]}
                {x}
            </>
        );
    });
}
