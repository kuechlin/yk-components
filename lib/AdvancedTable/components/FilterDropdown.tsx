import { FilterOutlined } from '@ant-design/icons';
import { Button, Card, Row, Space } from 'antd';
import React from 'react';

type Props = {
    onReset(): void;
    onOk(): void;
    icon?: React.ReactNode;
    children: React.ReactNode;
};
export const FilterDropdown: React.FC<Props> = ({
    onReset,
    onOk,
    icon,
    children,
}) => {
    return (
        <Card bordered size="small">
            <Space direction="vertical">
                <Row justify="space-between" style={{ minWidth: '200px' }}>
                    <Button
                        type="link"
                        onClick={onReset}
                        size="small"
                        style={{ width: 90 }}
                    >
                        Zurücksetzen
                    </Button>
                    <Button
                        type="primary"
                        onClick={onOk}
                        icon={icon ? icon : <FilterOutlined />}
                        size="small"
                        style={{ width: 90 }}
                    >
                        Ok
                    </Button>
                </Row>
                {children}
            </Space>
        </Card>
    );
};
