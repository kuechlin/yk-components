import { CaretDownFilled, CaretUpFilled } from '@ant-design/icons';
import { Button, Card, Checkbox, Col, Modal, Row, Typography } from 'antd';
import { useState } from 'react';
import { ColumnOptions } from '../types';

type ColumnSelectProps = {
    available: ColumnOptions<any>[];
    selected: ColumnOptions<any>[];
    onOk(selected: ColumnOptions<any>[], available: ColumnOptions<any>[]): void;
    onClose(): void;
};
export const ColumnSelect = ({
    onClose,
    onOk,
    ...props
}: ColumnSelectProps) => {
    const [state, setState] = useState({ ...props });

    const handleSelect =
        (type: 'select' | 'unselect') => (index: number) => () => {
            setState((draft) => {
                const available = Array.from(draft.available);
                const selected = Array.from(draft.selected);
                if (type === 'select') {
                    const [removed] = available.splice(index, 1);
                    selected.push(removed);
                } else {
                    const [removed] = selected.splice(index, 1);
                    available.push(removed);
                }
                return { available, selected };
            });
        };

    const handleMove = (type: 'up' | 'down', index: number) => () => {
        setState((draft) => {
            const selected = Array.from(draft.selected);
            const [removed] = selected.splice(index, 1);
            if (type === 'up') {
                selected.splice(index - 1, 0, removed);
            } else {
                selected.splice(index + 1, 0, removed);
            }
            return { selected, available: draft.available };
        });
    };

    const handleOk = () => onOk(state.selected, state.available);

    return (
        <Modal open onCancel={onClose} onOk={handleOk}>
            <Row>
                <Col span={12}>
                    <Typography.Title level={4}>
                        Verfügbare Spalten
                    </Typography.Title>
                </Col>
                <Col span={12}>
                    <Typography.Title level={4}>
                        Ausgewählte Spalten
                    </Typography.Title>
                </Col>
            </Row>
            <Row gutter={16}>
                <Col span={12}>
                    <ItemList
                        list={state.available}
                        onSelect={handleSelect('select')}
                    />
                </Col>
                <Col span={12}>
                    <ItemList
                        selected
                        list={state.selected}
                        onSelect={handleSelect('unselect')}
                        onMove={handleMove}
                    />
                </Col>
            </Row>
        </Modal>
    );
};

type ItemProps = {
    selected?: boolean;
    list: ColumnOptions<any>[];
    onSelect(index: number): () => void;
    onMove?(type: 'up' | 'down', index: number): () => void;
};
const ItemList = ({ list, onSelect, onMove, selected }: ItemProps) => {
    return (
        <Row gutter={[4, 4]}>
            {list.map((item, i) => (
                <Col span={24} key={i}>
                    <Card size="small" bodyStyle={{ padding: '4px' }}>
                        <Row gutter={8}>
                            <Col>
                                <Checkbox
                                    checked={selected}
                                    onClick={onSelect(i)}
                                />
                            </Col>
                            <Col flex="1">{item.title}</Col>
                            {onMove && (
                                <Col>
                                    <Button.Group size="small">
                                        <Button
                                            icon={<CaretUpFilled />}
                                            disabled={i === 0}
                                            onClick={onMove('up', i)}
                                        />
                                        <Button
                                            icon={<CaretDownFilled />}
                                            disabled={i === list.length - 1}
                                            onClick={onMove('down', i)}
                                        />
                                    </Button.Group>
                                </Col>
                            )}
                        </Row>
                    </Card>
                </Col>
            ))}
        </Row>
    );
};
