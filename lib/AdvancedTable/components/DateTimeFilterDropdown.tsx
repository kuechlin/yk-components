import { DatePicker, Select } from 'antd';
import { FilterDropdownProps, FilterValue } from 'antd/es/table/interface';
import dayjs, { Dayjs, isDayjs } from 'dayjs';
import { useAdvancedTableContext } from '../functions/context';
import { FilterDropdown } from './FilterDropdown';

type DateTimeFilterSingleOperator = 'eq' | 'gt' | 'lt' | 'neq';
export type DateTimeFilterState =
    | {
          op: DateTimeFilterSingleOperator;
          value: Dayjs | null;
      }
    | {
          op: 'range';
          values: [Dayjs | null, Dayjs | null];
      };

function toDate(val: any): Dayjs | null {
    if (!val) return null;
    else if (isDayjs(val)) return val;
    else return dayjs(val);
}

export function parseDateTimeState(
    value: FilterValue
): DateTimeFilterState | null {
    if (value && value.length > 0) {
        let obj = JSON.parse(String(value[0]));
        if (obj.op === 'range')
            return {
                op: 'range',
                values: [toDate(obj.values[0]), toDate(obj.values[1])],
            };
        else
            return {
                op: obj.op as DateTimeFilterSingleOperator,
                value: toDate(obj.value),
            };
    } else {
        return null;
    }
}
function toFilterValue(filter: DateTimeFilterState): string[] {
    return [JSON.stringify(filter)];
}

interface DateTimeFilterProps extends FilterDropdownProps {
    dataIndex: string;
}
export const DateTimeFilterDropdown = ({
    confirm,
    close,
    clearFilters,
    selectedKeys,
    setSelectedKeys,
    dataIndex,
}: DateTimeFilterProps) => {
    const [state, setState] = useAdvancedTableContext();
    const filterValue = parseDateTimeState(selectedKeys);

    const handleSearch = () => {
        confirm();
        const store = { ...state.store };
        if (store[dataIndex]) {
            store[dataIndex].filters = selectedKeys;
        }
        setState({ store });
    };

    const handleReset = () => {
        clearFilters && clearFilters();
        const store = { ...state.store };
        if (store[dataIndex]) {
            store[dataIndex].filters = null;
        }
        setState({ store });
        close();
    };

    const handleChange = (value: DateTimeFilterState) =>
        setSelectedKeys(toFilterValue(value));

    const handleFilterChange = (
        value: DateTimeFilterSingleOperator | 'range'
    ) => {
        if (value === 'range') {
            handleChange({
                op: value,
                values: [null, null],
            });
        } else {
            handleChange({
                op: value,
                value: null,
            });
        }
    };

    return (
        <FilterDropdown onOk={handleSearch} onReset={handleReset}>
            <Select
                value={filterValue?.op}
                options={[
                    { value: 'eq', label: 'gleich' },
                    { value: 'neq', label: 'nicht gelich' },
                    { value: 'gt', label: 'größer als' },
                    { value: 'lt', label: 'kleiner als' },
                    { value: 'range', label: 'zwischen' },
                ]}
                style={{ width: '100%' }}
                placeholder="Filtertyp"
                onChange={handleFilterChange}
            />
            {filterValue?.op === 'range' ? (
                <DatePicker.RangePicker
                    value={filterValue.values}
                    placeholder={['Von', 'Bis']}
                    onChange={(val) =>
                        handleChange({
                            op: 'range',
                            values: val || [null, null],
                        })
                    }
                    format="DD.MM.YYYY"
                />
            ) : (
                <DatePicker
                    value={filterValue?.value}
                    onChange={(value) =>
                        handleChange({
                            op: filterValue?.op as DateTimeFilterSingleOperator,
                            value,
                        })
                    }
                    placeholder="Wert"
                    format="DD.MM.YYYY"
                    style={{ width: '100%' }}
                />
            )}
        </FilterDropdown>
    );
};
