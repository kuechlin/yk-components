import React, { useRef, useState } from 'react';
import { useAdvancedTableContext } from '../functions/context';

export interface TableTitleProps extends React.HTMLProps<any> {
    id: string;
    width: number;
}
export const TableHeaderCell = ({
    id,
    width,
    children,
    ...rest
}: TableTitleProps) => {
    const [state, setState] = useAdvancedTableContext();

    const handleResize = (width: number) => {
        const store = { ...state.store };
        if (store[id]) {
            store[id].width = width;
        }
        setState({ store });
    };

    return (
        <th
            {...rest}
            style={{
                ...rest.style,
                userSelect: 'none',
                position: 'relative',
                minWidth: width,
            }}
        >
            <Resizer onResize={handleResize} />
            {children}
        </th>
    );
};

type ResizerProps = {
    onResize(width: number): void;
};
const Resizer = ({ onResize }: ResizerProps) => {
    const [active, setActive] = useState<boolean>(false);
    const state = useRef<{ clientX: number; width: number }>({
        clientX: 0,
        width: 0,
    });
    const ref = useRef<HTMLDivElement>(null);

    const handleMouseUp = (e: MouseEvent) => {
        e.stopPropagation();
        window.removeEventListener('mousemove', handleMouseMove);
        window.removeEventListener('mouseup', handleMouseUp);
        setActive(false);
        onResize(state.current.width);
        if (ref.current) {
            ref.current.style.width = '100%';
        }
    };
    const handleMouseMove = (e: MouseEvent) => {
        e.stopPropagation();
        const delta = e.clientX - state.current.clientX;
        state.current.clientX = e.clientX;
        state.current.width += delta;

        if (ref.current) {
            ref.current.style.width = state.current.width + 'px';
        }
    };

    const handleMouseDown = (e: React.MouseEvent) => {
        e.stopPropagation();
        if (ref.current) {
            setActive(true);
            state.current.clientX = e.clientX;
            state.current.width = ref.current.getBoundingClientRect().width;

            window.addEventListener('mousemove', handleMouseMove);
            window.addEventListener('mouseup', handleMouseUp);
        }
    };

    return (
        <div
            ref={ref}
            style={{
                position: 'absolute',
                left: 0,
                top: 0,
                height: 'calc(100% + 2px)',
                width: '100%',
                border: active ? '1px dotted #177ddc' : '',
                margin: '-1px',
                zIndex: active ? 99 : undefined,
            }}
        >
            <span
                style={{
                    position: 'absolute',
                    boxSizing: 'border-box',
                    zIndex: 10,
                    top: 0,
                    right: 0,
                    height: '100%',
                    width: '4px',
                    padding: '0px 2px',
                    cursor: 'col-resize',
                }}
                onClick={(e) => e.stopPropagation()}
                onMouseDown={handleMouseDown}
            />
        </div>
    );
};
