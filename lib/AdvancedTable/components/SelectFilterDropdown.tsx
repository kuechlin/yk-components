import { Select } from 'antd';
import { FilterDropdownProps } from 'antd/es/table/interface';
import React, { useMemo } from 'react';
import { get, getNodeText } from '../../utils';
import { useAdvancedTableContext } from '../functions/context';
import { FilterDropdown } from './FilterDropdown';

interface SelectFilterProps extends FilterDropdownProps {
    dataIndex: string;
    render?: (val: any, r: any, i: number) => React.ReactNode;
    value?: (r: any) => any;
}
export const SelectFilterDropdown = ({
    confirm,
    clearFilters,
    selectedKeys,
    setSelectedKeys,
    dataIndex,
    render,
    value,
}: SelectFilterProps) => {
    const [state, setState] = useAdvancedTableContext();

    const handleOk = () => {
        confirm();
        const store = { ...state.store };
        if (store[dataIndex]) {
            store[dataIndex].filters = selectedKeys;
        }
        setState({ store });
    };

    const handleReset = () => {
        clearFilters && clearFilters();
        const store = { ...state.store };
        if (store[dataIndex]) {
            store[dataIndex].filters = null;
        }
        setState({ store });
    };

    const filters = useMemo(() => {
        const values: Record<string, any> = {};

        for (let i = 0; i < state.dataSource.length; i++) {
            const record = state.dataSource[i];
            const val = value ? value(record) : get(record, dataIndex);
            const label = render ? render(val, record, i) : val;

            values[String(val)] = label;
        }

        return Object.entries(values)
            .sort((a, b) => (a[0] > b[0] ? 1 : -1))
            .map(([value, label]) => ({
                value,
                label,
            }));
    }, [state.dataSource]);

    return (
        <FilterDropdown onOk={handleOk} onReset={handleReset}>
            <Select
                mode="multiple"
                value={selectedKeys}
                onChange={(e) => setSelectedKeys(e)}
                style={{ width: '100%' }}
                showSearch
                placeholder="Bitte Wählen"
                options={filters}
                filterOption={(input, option) =>
                    getNodeText(option?.label ?? '')
                        .toLowerCase()
                        .includes(input.toLowerCase())
                }
            />
        </FilterDropdown>
    );
};
