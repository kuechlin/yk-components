import { SearchOutlined } from '@ant-design/icons';
import { Input, InputRef } from 'antd';
import { FilterDropdownProps } from 'antd/es/table/interface';
import { useAdvancedTableContext } from '../functions/context';
import { FilterDropdown } from './FilterDropdown';

interface SearchFilterProps extends FilterDropdownProps {
    dataIndex: string;
    setInputRef(input: InputRef): void;
}
export const SearchFilterDropdown = ({
    confirm,
    close,
    clearFilters,
    selectedKeys,
    setSelectedKeys,
    dataIndex,
    setInputRef,
}: SearchFilterProps) => {
    const [state, setState] = useAdvancedTableContext();

    const handleOk = () => {
        const store = { ...state.store };
        if (store[dataIndex]) {
            store[dataIndex].filters = selectedKeys;
        }
        setState({ store });
        confirm();
    };

    const handleReset = () => {
        clearFilters && clearFilters();
        const store = { ...state.store };
        if (store[dataIndex]) {
            store[dataIndex].filters = null;
        }
        setState({ store });
        close();
    };

    return (
        <FilterDropdown onOk={handleOk} onReset={handleReset}>
            <Input
                ref={setInputRef}
                placeholder="Suchen"
                value={selectedKeys[0]}
                onChange={(e) =>
                    setSelectedKeys(e.target.value ? [e.target.value] : [])
                }
                onPressEnter={handleOk}
                style={{ width: '100%' }}
                addonAfter={<SearchOutlined />}
            />
        </FilterDropdown>
    );
};
