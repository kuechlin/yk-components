import { ColumnProps } from 'antd/es/table';
import { get } from '../../utils';

export const applySorter = <T>(
    props: ColumnProps<T>,
    dataIndex: string | string,
    value?: (r: T) => any
) => {
    props.sorter = (a: T, b: T): number => {
        let valA, valB;
        if (value) {
            valA = value(a);
            valB = value(b);
        } else {
            valA = get(a, dataIndex);
            valB = get(b, dataIndex);
        }

        if (typeof valA === 'string' && typeof valB === 'string') {
            return valA.localeCompare(valB);
        } else if (typeof valA === 'number' && typeof valB === 'number') {
            return valA - valB;
        } else {
            return valA > valB ? 1 : -1;
        }
    };
    props.sortDirections = ['ascend', 'descend'];
    props.defaultSortOrder = props.defaultSortOrder;
};
