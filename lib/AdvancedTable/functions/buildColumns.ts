import { ColumnProps } from 'antd/es/table';
import { get } from '../../utils';
import {
    ColumnOptions,
    FilterSearchOptions,
    FilterSelectOptions,
} from '../types';
import { applyDateTimeFilter } from './applyDateTimeFilter';
import { applyNumberFilter } from './applyNumberFilter';
import { applySearchFilter } from './applySearchFilter';
import { applySelectFilter } from './applySelectFilter';
import { applySorter } from './applySorter';
import { ColumnState, StoreColumn } from './storage';
import { applyBoolFilter } from './applyBoolFilter';

const getColumnWidth = <T>(col: ColumnOptions<T>) => {
    if (col.width === 'small') {
        return 50;
    } else if (col.width === 'medium') {
        return 100;
    } else if (col.width === 'large') {
        return 200;
    } else {
        return col.width;
    }
};

export const buildColumn =
    <T extends Object>(store: ColumnState) =>
    (column: ColumnOptions<T>): ColumnProps<T> => {
        const { sorter, filter, dataIndex, render, value, ...rest } = column;
        const { filters, sortOrder, width } =
            store[dataIndex] || ({} as Partial<StoreColumn>);
        const props: ColumnProps<T> = {
            dataIndex: dataIndex,
            onHeaderCell: (data: any) => ({
                id: data.dataIndex,
                width: data.width,
            }),
            ...rest,
            width: width || getColumnWidth(column),
            filteredValue: filters,
            filtered: !!filters,
            sortOrder: sortOrder,
        };

        if (render) {
            props.render = (_v, e, i) => render(get(e, dataIndex), e, i);
        }
        if (sorter !== false) {
            applySorter(props, dataIndex, value);
        }
        if (filter) {
            const filterType =
                typeof filter === 'object' ? filter.type : filter;

            switch (filterType) {
                case 'select':
                    applySelectFilter(
                        props,
                        dataIndex,
                        filter as FilterSelectOptions<T>,
                        value
                    );
                    break;
                case 'search':
                    applySearchFilter(
                        props,
                        dataIndex,
                        filter as FilterSearchOptions<T>,
                        value
                    );
                    break;
                case 'number':
                    applyNumberFilter(props, dataIndex, value);
                    break;
                case 'datetime':
                    applyDateTimeFilter(props, dataIndex, value);
                    break;
                case 'bool':
                    applyBoolFilter(props, dataIndex, value);
            }
        }
        return props;
    };

