import { TableColumnType } from 'antd';
import { ColumnProps } from 'antd/es/table';

const checkItem = <T>(item: T, col: ColumnProps<T>) => {
    if (col && col.onFilter && col.filtered && col.filteredValue) {
        for (const value of col.filteredValue) {
            if (!col.onFilter(value, item)) {
                return false;
            }
        }
    }
    return true;
};

export const filterData = <T>(
    selected: TableColumnType<any>[],
    dataSource: T[]
) => {
    var result = [];
    for (const item of dataSource) {
        let include = true;
        for (const column of selected) {
            if (!checkItem(item, column)) {
                include = false;
            }
        }
        if (include) result.push(item);
    }
    return result;
};
