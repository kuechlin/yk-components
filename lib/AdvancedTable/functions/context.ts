import { createContext, useContext } from 'react';
import { ColumnState } from './storage';

export type AdvancedTableState = {
    dataSource: any[];
    store: ColumnState;
};

export type AdvancedTableContextState = [
    AdvancedTableState,
    (state: Partial<AdvancedTableState>) => void
];

export const AdvancedTableContext = createContext<AdvancedTableContextState>(
    [] as any
);

export const useAdvancedTableContext = () => useContext(AdvancedTableContext);
