import { ColumnProps } from 'antd/es/table';
import { get, isNumber } from '../../utils';
import {
    NumberFilterDropdown,
    parseNumberState,
} from '../components/NumberFilterDropdown';

export type NumberFilterOptions<T> = {
    type: 'number';
    value?: (record: T) => number;
};

export const applyNumberFilter = <T extends Object>(
    props: ColumnProps<T>,
    dataIndex: string,
    value?: (r: T) => any
) => {
    props.filterDropdown = (props) => (
        <NumberFilterDropdown {...props} dataIndex={dataIndex} />
    );

    props.onFilter = (v, record) => {
        let state = parseNumberState([v]);
        // Get value from record
        let val: number;
        if (value) {
            val = value(record);
        } else {
            val = get(record, dataIndex);
        }
        // check if value exists
        if (!state || !isNumber(val)) return false;
        /* Filter */
        if (state.op === 'range') {
            if (isNumber(state.values[0]) && isNumber(state.values[1])) {
                return state.values[0] <= val && val <= state.values[1];
            } else if (isNumber(state.values[0])) {
                return state.values[0] <= val;
            } else if (isNumber(state.values[1])) {
                return val <= state.values[1];
            }
        } else {
            if (!isNumber(state.value)) return false;
            switch (state.op) {
                case 'eq':
                    return val === state.value;
                case 'neq':
                    return val !== state.value;
                case 'gt':
                    return val > state.value;
                case 'lt':
                    return val < state.value;
            }
        }
        return false;
    };

    return props;
};
