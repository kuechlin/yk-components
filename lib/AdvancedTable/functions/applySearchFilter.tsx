import { SearchOutlined } from '@ant-design/icons';
import { InputRef } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { get } from '../../utils';
import { FilterHighlighter } from '../components/FilterHighlighter';
import { SearchFilterDropdown } from '../components/SearchFilterDropdown';
import { FilterSearchOptions } from '../types';

export const applySearchFilter = <T extends Object>(
    props: ColumnProps<T>,
    dataIndex: string,
    filter: FilterSearchOptions<T>,
    value?: (r: T) => any
) => {
    var searchInput: InputRef | null = null;
    var render = props.render;

    props.filterDropdown = (props) => (
        <SearchFilterDropdown
            {...props}
            dataIndex={dataIndex}
            setInputRef={(ref) => {
                searchInput = ref;
            }}
        />
    );
    props.filterIcon = (filtered) => (
        <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />
    );
    props.onFilter = (v: any, record) => {
        let val = value ? value(record) : get(record, dataIndex)
        if (val) {
            return val.toLowerCase().includes(v.toLowerCase());
        } else return false;
    };
    props.onFilterDropdownOpenChange = (visible) => {
        if (visible) {
            setTimeout(() => searchInput && searchInput.select());
        }
    };
    if (typeof filter !== 'object' || filter.noHighlight !== true)
        props.render = (val, ele, i) => (
            <FilterHighlighter
                dataIndex={dataIndex}
                children={render ? render(val, ele, i) : get(ele, dataIndex)}
            />
        );
};
