import { FilterValue, SortOrder } from 'antd/es/table/interface';
import { ColumnOptions } from '../types';

export type StorageConfig = {
    id: string;
};
export type StoreColumn = {
    selected: boolean;
    index: number;
    filters?: FilterValue | null;
    sortOrder: SortOrder;
    width?: number;
};

export type ColumnState = Record<string, StoreColumn>;

export const getStorageState = (
    columns: ColumnOptions<any>[],
    defaultSelected?: string[],
    config?: StorageConfig
): ColumnState => {
    // use local state
    var result: ColumnState = getItem(config);
    // No Local state
    for (let i = 0; i < columns.length; i++) {
        const col = columns[i];
        if (!defaultSelected || defaultSelected.includes(col.dataIndex)) {
            result[col.dataIndex] = {
                selected: true,
                index: defaultSelected
                    ? defaultSelected.indexOf(col.dataIndex)
                    : i,
                filters: null,
                sortOrder: null,
                ...(result[col.dataIndex] as any),
            };
        }
    }
    return result;
};

const getItem = (config?: StorageConfig): ColumnState => {
    if (!config) return {};
    try {
        const local = localStorage.getItem(config.id);
        const session = sessionStorage.getItem(config.id);
        var result: any = {};
        const addValues = (values: Record<string, any>) => {
            for (const [key, value] of Object.entries(values)) {
                result[key] = {
                    ...result[key],
                    ...value,
                };
            }
        };
        if (session) {
            addValues(JSON.parse(session));
        }
        if (local) {
            addValues(JSON.parse(local));
        }

        return result;
    } catch (error) {
        return {};
    }
};

export const setStorageState = (state: ColumnState, config?: StorageConfig) => {
    if (!config) return;
    var local: Record<string, any> = {};
    var session: Record<string, any> = {};
    for (const [
        key,
        { index, filters, selected, sortOrder, width },
    ] of Object.entries(state)) {
        local[key] = {
            index,
            sortOrder,
            selected,
            width,
        };
        session[key] = {
            filters,
        };
    }
    localStorage.setItem(config.id, JSON.stringify(local));
    sessionStorage.setItem(config.id, JSON.stringify(session));
};

type PafinationStore = {
    page: number;
    pageSize: number | undefined;
};
export const getLocalPagination = (
    config?: StorageConfig
): Partial<PafinationStore> => {
    if (!config) return {};
    try {
        const page = sessionStorage.getItem(config.id + '/pagination/page');
        const pageSize = localStorage.getItem(
            config.id + '/pagination/pageSize'
        );
        return {
            page: page ? +page : undefined,
            pageSize: pageSize ? +pageSize : undefined,
        };
    } catch (error) {
        return {};
    }
};

export const setLocalPagination = (
    { page, pageSize }: PafinationStore,
    config?: StorageConfig
) => {
    if (!config) return;
    sessionStorage.setItem(config.id + '/pagination/page', page.toString());
    if (pageSize) {
        localStorage.setItem(
            config.id + '/pagination/pageSize',
            pageSize.toString()
        );
    }
};
