import { ColumnProps } from 'antd/es/table';
import { BoolFilterDropdown } from '../components/BoolFilterDropdown';
import { get } from '../../utils';

export type FilterBoolOptions<T> = {
    type: 'bool';
    value?: (record: T) => boolean;
};
export const applyBoolFilter = <T extends Object>(
    props: ColumnProps<T>,
    dataIndex: string,
    value?: (r: T) => any
) => {
    const render = props.render;

    props.filterDropdown = (props) => (
        <BoolFilterDropdown {...props} dataIndex={dataIndex} />
    );

    props.onFilter = (v, record) => {
        let val = get(record, dataIndex);
        if (value) {
            val = value(record);
        }
        return getBool(v.toString()) === val;
    };

    props.render = (v, r, i) => {
        if (render) return render(v, r, i);
        const val = get(r, dataIndex);
        if (val === true) return 'Ja';
        else return 'Nein';
    };

    return props;
};

function getBool(value: string) {
    return value === 'true';
}