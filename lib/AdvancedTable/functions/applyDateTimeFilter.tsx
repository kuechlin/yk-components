import { ColumnProps } from 'antd/es/table';
import dayjs, { Dayjs, isDayjs } from 'dayjs';
import isBetween from 'dayjs/plugin/isBetween';
import { get } from '../../utils';
import {
    DateTimeFilterDropdown,
    parseDateTimeState,
} from '../components/DateTimeFilterDropdown';
dayjs.extend(isBetween);

export type DateTimeFilterOptions<T> = {
    type: 'datatime';
    value?: (record: T) => Dayjs;
};

export const applyDateTimeFilter = <T extends Object>(
    props: ColumnProps<T>,
    dataIndex: string,
    value?: (r: T) => any
) => {
    const render = props.render;

    props.filterDropdown = (props) => (
        <DateTimeFilterDropdown {...props} dataIndex={dataIndex} />
    );

    props.onFilter = (v, record) => {
        let state = parseDateTimeState([v]);
        // Get value from record
        let val: Dayjs | null;
        if (value) {
            val = dayjs(value(record));
        } else {
            val = dayjs(get(record, dataIndex));
        }
        // check if value exists
        if (!state || !val.isValid()) return false;
        // Filter
        if (state.op === 'range') {
            if (val && state.values[0] && state.values[1]) {
                return val.isBetween(
                    state.values[0],
                    state.values[1],
                    'date',
                    '[]'
                );
            }
        } else {
            if (!isDayjs(state.value) || !state.value.isValid()) return false;
            switch (state.op) {
                case 'eq':
                    return val.isSame(state.value, 'date');
                case 'neq':
                    return !val.isSame(state.value, 'date');
                case 'gt':
                    return val.isAfter(state.value, 'date');
                case 'lt':
                    return val.isBefore(val, 'date');
            }
        }
        return false;
    };

    props.render = (v, r, i) => {
        if (render) return render(v, r, i);
        const val = get(r, dataIndex);
        if (isDayjs(val) || val instanceof Date) {
            return val.toLocaleString();
        } else {
            return val;
        }
    };

    return props;
};
