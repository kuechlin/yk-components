import { ColumnProps } from 'antd/es/table';
import { get } from '../../utils';
import { SelectFilterDropdown } from '../components/SelectFilterDropdown';
import { FilterSelectOptions } from '../types';

export const applySelectFilter = <T extends Object>(
    props: ColumnProps<T>,
    dataIndex: string,
    filter: FilterSelectOptions<T>,
    value?: (r: any) => any
) => {
    props.filterDropdown = (_props) => (
        <SelectFilterDropdown
            {..._props}
            dataIndex={dataIndex}
            render={
                typeof filter === 'object' && filter.renderOption
                    ? filter.renderOption
                    : (props.render as any)
            }
            value={value}
        />
    );

    props.onFilter = (v, record) => {
        const val = value ? value(record) : get(record, dataIndex);
        if (val) {
            return String(val) === String(v);
        } else return false;
    };

    props.filterMultiple = true;
};
