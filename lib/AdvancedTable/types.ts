import { TableColumnType, TableProps } from 'antd';
import { StorageConfig } from './functions/storage';

export interface AdvancedTableProps<T = any>
    extends Omit<TableProps<T>, 'dataSource' | 'columns' | 'pagination'> {
    dataSource: T[];
    columns: ColumnOptions<T>[];
    defaultSelected?: string[];
    actionColumn?: TableColumnType<T>;
    storage?: StorageConfig;
    pagination?: false | { page: number; pageSize: number };
}

export interface ColumnOptions<T>
    extends Omit<
        TableColumnType<T>,
        'dataIndex' | 'title' | 'sorter' | 'sortDirections' | 'width' | 'render'
    > {
    title: string;
    dataIndex: string;
    hidden?: boolean;
    sorter?: false;
    filter?: FilterOptions<T>;

    value?: (r: T) => any;
    width?: 'small' | 'medium' | 'large' | number;
    render?: (value: any, record: T, index: number) => React.ReactNode;
}

export type FilterOptions<T> =
    | 'number'
    | 'datetime'
    | 'bool'
    | FilterSelectOptions<T>
    | FilterSearchOptions<T>;
export type FilterSelectOptions<T> =
    | 'select'
    | {
          type: 'select';
          renderOption?: (
              value: any,
              record: T,
              index: number
          ) => React.ReactNode;
      };
export type FilterSearchOptions<T> =
    | 'search'
    | { type: 'search'; noHighlight?: boolean };

export function columnOptions<T>(o: ColumnOptions<T>) {
    return o;
}

export type AdvancedTableHandle = {
    openColumnSelect(): void;
    getFilteredData(): any[];
    clearAllFilters(): void;
};
