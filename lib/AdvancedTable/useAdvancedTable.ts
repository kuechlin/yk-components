import { useCallback, useRef } from 'react';
import { AdvancedTableHandle } from './types';

type AdvancedTableState<T> = {
    ref: React.RefObject<AdvancedTableHandle>;
    openColumnSelect(): void;
    getFilteredData(): T[];
    clearAllFilters(): void;
};
export const useAdvancedTable = <T>(): AdvancedTableState<T> => {
    const ref = useRef<AdvancedTableHandle>(null);

    const openColumnSelect = useCallback(() => {
        if (ref.current) {
            ref.current.openColumnSelect();
        }
    }, [ref.current]);
    const getFilteredData = useCallback(() => {
        if (ref.current) {
            return ref.current.getFilteredData();
        } else {
            return [];
        }
    }, [ref.current]);
    const clearAllFilters = useCallback(() => {
        if (ref.current) {
            return ref.current.clearAllFilters();
        }
    }, [ref.current]);

    return {
        ref,
        openColumnSelect,
        getFilteredData,
        clearAllFilters,
    };
};
