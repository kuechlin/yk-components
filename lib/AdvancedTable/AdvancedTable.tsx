import { Table, TableColumnType, TablePaginationConfig } from 'antd';
import { FilterValue, SorterResult } from 'antd/es/table/interface';
import React, {
    forwardRef,
    useCallback,
    useImperativeHandle,
    useMemo,
    useState,
} from 'react';
import { ColumnSelect } from './components/ColumnSelect';
import { TableHeaderCell } from './components/TableHeaderCell';
import { buildColumn } from './functions/buildColumns';
import { AdvancedTableContext } from './functions/context';
import { filterData } from './functions/filterData';
import {
    ColumnState,
    getLocalPagination,
    getStorageState,
    setLocalPagination,
    setStorageState,
} from './functions/storage';
import {
    AdvancedTableHandle,
    AdvancedTableProps,
    ColumnOptions,
} from './types';

const defaultActionProps: TableColumnType<any> = {
    width: 50,
    fixed: 'right',
};

interface AdvancedState {
    open: boolean;
    page: number;
    pageSize: number;
    store: ColumnState;
}
const InternalTable = forwardRef<AdvancedTableHandle, AdvancedTableProps>(
    (
        {
            dataSource,
            columns: rawColumns,
            defaultSelected,
            actionColumn,
            pagination,
            storage,
            ...props
        },
        ref
    ) => {
        // filter out hidden columns
        const allColumns = useMemo(
            () => rawColumns.filter((a) => !a.hidden),
            [rawColumns]
        );
        // initialize state
        const [state, setState] = useState<AdvancedState>({
            open: false,
            page: (pagination && pagination.page) || 1,
            pageSize: (pagination && pagination.pageSize) || 50,
            store: getStorageState(rawColumns, defaultSelected, storage),
            ...getLocalPagination(storage),
        });
        // get selected columns
        const selected = useMemo(
            () =>
                allColumns
                    .filter(
                        (a) =>
                            state.store[a.dataIndex] &&
                            state.store[a.dataIndex].selected
                    )
                    .sort(
                        (a, b) =>
                            state.store[a.dataIndex].index -
                            state.store[b.dataIndex].index
                    )
                    .map(buildColumn(state.store)),
            [allColumns, state.store]
        );
        // get all available columns
        const available = useMemo(
            () =>
                allColumns.filter(
                    (a) =>
                        !(
                            state.store[a.dataIndex] &&
                            state.store[a.dataIndex].selected
                        )
                ),
            [allColumns, state.store]
        );
        // update state and set local state
        const updateState = useCallback(
            (partial: Partial<AdvancedState>) => {
                setState((next) => ({ ...next, ...partial }));
                if (partial.store) {
                    setStorageState(partial.store, storage);
                }
            },
            [storage]
        );
        // create imperative handle witch public functions
        useImperativeHandle(ref, () => ({
            // open column select dialog
            openColumnSelect: () => updateState({ open: true }),
            // get filtered table values
            getFilteredData: () => filterData(selected, dataSource),
            // clear all filters
            clearAllFilters: () => {
                const store = { ...state.store };
                for (const key in store) {
                    store[key].sortOrder = null;
                    store[key].filters = null;
                }
                updateState({ store });
            },
        }));

        // apply select
        const handleOk = (
            selected: ColumnOptions<any>[],
            available: ColumnOptions<any>[]
        ) => {
            var store = { ...state.store };
            for (let i = 0; i < selected.length; i++) {
                const item = selected[i];
                store[item.dataIndex] = {
                    ...store[item.dataIndex],
                    selected: true,
                    index: i,
                };
            }
            for (const item of available) {
                if (store[item.dataIndex]) {
                    store[item.dataIndex].selected = false;
                }
            }
            updateState({
                open: false,
                store,
            });
        };
        // close select dialog
        const handleClose = useCallback(
            () => updateState({ open: false }),
            [updateState]
        );
        // update pagination
        const handlePagination = useCallback(
            (page: number, pageSize: number | undefined) => {
                setLocalPagination({ page, pageSize }, storage);
                updateState({ page, pageSize });
            },
            [updateState]
        );
        // update sort order
        const handleChange = (
            pagination: TablePaginationConfig,
            _filters: Record<string, FilterValue | null>,
            sorter: SorterResult<any> | SorterResult<any>[]
        ) => {
            if (pagination) {
                updateState({
                    page: pagination.current,
                    pageSize: pagination.pageSize,
                });
            }
            if (sorter) {
                const store = { ...state.store };
                for (const key in store) {
                    store[key].sortOrder = null;
                }
                if (Array.isArray(sorter)) {
                    for (const item of sorter) {
                        store[item.field as any].sortOrder = item.order || null;
                    }
                } else {
                    store[sorter.field as any].sortOrder = sorter.order || null;
                }
                updateState({
                    store,
                });
            }
        };
        // build selected columns
        const columns = useMemo(() => {
            var columns = [...selected];
            if (actionColumn) {
                columns.push({
                    ...defaultActionProps,
                    ...actionColumn,
                });
            }
            return columns;
        }, [selected, actionColumn]);

        return (
            <AdvancedTableContext.Provider
                value={[{ dataSource, store: state.store }, updateState]}
            >
                <Table
                    {...props}
                    components={{
                        ...props.components,
                        header: {
                            ...props.components?.header,
                            cell: TableHeaderCell,
                        },
                    }}
                    dataSource={dataSource}
                    columns={columns}
                    onChange={handleChange}
                    pagination={
                        pagination === false
                            ? false
                            : {
                                  ...pagination,
                                  pageSize: state.pageSize,
                                  current: state.page,
                                  onChange: handlePagination,
                              }
                    }
                />

                {state.open && (
                    <ColumnSelect
                        selected={selected as any}
                        available={available}
                        onOk={handleOk}
                        onClose={handleClose}
                    />
                )}
            </AdvancedTableContext.Provider>
        );
    }
);

InternalTable.defaultProps = {
    rowKey: 'id',
    pagination: { pageSize: 25, page: 1 },
};

export const AdvancedTable = InternalTable as <T = any>(
    props: AdvancedTableProps<T> & { ref?: React.Ref<AdvancedTableHandle> }
) => React.ReactElement;
