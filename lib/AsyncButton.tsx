import { Button, ButtonProps } from 'antd';
import { useState } from 'react';

interface AsyncButtonProps extends Omit<ButtonProps, 'onClick' | 'loading'> {
    onClick(): any;
}
export const AsyncButton = ({ onClick, ...props }: AsyncButtonProps) => {
    const [loading, setLoading] = useState(false);

    const handleClick = async () => {
        try {
            setLoading(true);
            const promise = onClick();
            if (promise && promise instanceof Promise) {
                await promise;
            }
        } catch (error) {
        } finally {
            setLoading(false);
        }
    };

    return <Button {...props} loading={loading} onClick={handleClick} />;
};
